package com.phf.test;

import com.alibaba.druid.sql.visitor.functions.If;
import com.phf.entity.Admin;
import com.phf.entity.Category;
import com.phf.mapper.AdminMapper;
import com.phf.mapper.CategoryMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class MapperTest {
@Autowired
    private AdminMapper adminMapper;
@Test
    public void testSelectAll(){
    List<Admin> admins=adminMapper.selectAdmins();
    for (Admin admin:admins){
        System.out.println("admin="+admin);
    }

}
@Test
    public void  testInsertAdmin(){
    Admin admin =new Admin(null,"xiaohei","123456");
    adminMapper.insertAdmin(admin);
    System.out.println("admin="+admin);
}
@Test
    public void testDeleteAdmin(){
    adminMapper.deleteAdmin(2);

}
@Test
    public void testSelectAdmin(){
    adminMapper.selectAdmin(1);


}
@Test
    public void testUpdateAdmin(){
    Admin admin=new Admin(2,"xiaoming","123");
    adminMapper.updateAdmin(admin);
    System.out.println(admin);

}
@Test
    public  void testSelectAdminByUsername(){
    Admin admin=adminMapper.selectAdminByUsername("xiaohei");
    System.out.println("admin="+admin);

}
@Autowired
    private CategoryMapper categoryMapper;
@Test
    public void testAddCategory(){
    Category c=new Category(null,"玄幻小说",2,1,null,null);
    categoryMapper.insertCategory(c);
    System.out.println(c);
}
@Test
    public void testDeleteCategory(){
    categoryMapper.deleteCategory(2);
}
@Test
    public void testSelectcategoryWithParent(){


}
@Test
    public void test(){
//
    System.out.println("===============");
   int i=0,j=9;

   do {
       if(i++>--j)
           break;

   }

    while (i<4);
    System.out.println("i="+i+"j="+j);
}

}
