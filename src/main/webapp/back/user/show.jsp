<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width" initial-scale="1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
</head>
<body>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4>用户管理</h4>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>id</th>
                <th>昵称</th>
                <th>邮箱</th>
                <th>密码</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="u" items="${pageInfo.list}">
                <tr>
                    <td>${u.userId}</td>
                    <td>${u.nickname}</td>
                    <td>${u.email}</td>
                    <td>${u.password}</td>
                    <td>${u.status == 1 ? "正常":"锁定"}</td>
                    <td>
                        <a href="${pageContext.request.contextPath}/back/user/updateStatus.do?userId=${u.userId}&status=${u.status == 1 ?-1:1}" class="btn btn-warning">修改状态</a>
                    </td>
                </tr>
            </c:forEach>

            </tbody>
        </table>

        <ul class="pagination">
            <c:if test="${pageInfo.pageNum == 1}">
                <li class="disabled">
                    <a href="javascript:void(0)" aria-label="Previous" >
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            </c:if>
            <c:if test="${pageInfo.pageNum > 1}">
                <li>
                    <a href="${pageContext.request.contextPath}/back/user/show.do?pageNum=${pageInfo.pageNum-1}&pageSize=${pageInfo.pageSize}" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            </c:if>
            <c:forEach var="i"  items="${requestScope.pageInfo.navigatepageNums}">
                <li ${pageInfo.pageNum == i? "class='active'":""}><a href="${pageContext.request.contextPath}/back/user/show.do?pageNum=${i}&pageSize=${pageInfo.pageSize}">${i}</a></li>
            </c:forEach>
            <!--
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            -->
            <c:if test="${pageInfo.isLastPage}">
                <li class="disabled">
                    <a href="javascript:void(0)" aria-label="Previous" class="disabled">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </c:if>
            <c:if test="${!pageInfo.isLastPage}">
                <li>
                    <a href="${pageContext.request.contextPath}/back/user/show.do?pageNum=${pageInfo.pageNum+1}&pageSize=${pageInfo.pageSize}" aria-label="Previous">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </c:if>
        </ul>
    </div>
    <div class="panel-footer"></div>
</div>
<script>

</script>
</body>
</html>
