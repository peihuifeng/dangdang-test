<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Xsy
  Date: 2020/7/12
  Time: 11:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
    <style type="text/css">


        #file_upload1 {
            display: none;
        }
        #file_upload1_label {
            display: inline-block;
            border: 1px solid #aaa;
            width: 120px;
            height: 145px;
            margin-left: 20px;
            text-align: center;
            line-height: 145px;
            cursor: pointer;
        }
    </style>


</head>

<body>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="text-left">修改图书信息</h4>
    </div>
    <div class="panel-body" >
        <form action="${pageContext.request.contextPath}/back/book/update.do" method="post" enctype="multipart/form-data" class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-sm-2">名称：</label>
                <div class="col-sm-4">
                    <input type="hidden" name="bookId" value="${book.bookId}"/>
                    <input type="text" name="bookName" class="form-control" value="${book.bookName}"></div>

                <label class="control-label col-sm-2">页数：</label>
                <div class="col-sm-4"><input type="number" name="pageNum" class="form-control" value="${book.pageNum}" ></div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">所属分类：</label>
                <div class="col-sm-4">
                    <select name="category.id" class="form-control inner2">
                        <c:forEach var="c" items="${categories}">
                            <option value="${c.categoryId}" ${book.categoryId == c.categoryId? "selected":""}>${c.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <label class="control-label col-sm-2">字数：</label>
                <div class="col-sm-4"><input type="number" name="wordNum" class="form-control" value="${book.wordNum}" ></div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-4">原价：</label>
                        <div class="col-sm-8"><input type="number" name="price" class="form-control" value="${book.price}"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">当当价：</label>
                        <div class="col-sm-8"><input type=number name="dprice" class="form-control" value="${book.dprice}"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">库存：</label>
                        <div class="col-sm-8"><input type="number" name="stock" class="form-control" value="${book.stock}"></div>
                    </div>

                </div>

                <label class="control-label col-sm-2">封面：</label>
                <div class="col-sm-4">
                    <label id="file_upload1_label" for="file_upload1">
                        <img id="uploadimg" src="${pageContext.request.contextPath}/images/book/${book.cover}" alt="" width="120" height="145" />
                    </label>
                    <input type="file" name="pic" class="" id="file_upload1" onchange="upload_review()">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-4">作者：</label>
                        <div class="col-sm-8"><input type="text" name="author" class="form-control" value="${book.author}"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">出版社：</label></td>
                        <div class="col-sm-8"><input type="text" name="press" class="form-control" value="${book.press}"></div>
                    </div>
                </div>

                <label class="control-label col-sm-2">编辑推荐：</label>
                <div class="col-sm-4"><textarea class="form-control" name="editorRecommend">${book.editorRecommend}</textarea></div>
            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-4">出版时间：</label>
                        <div class="col-sm-8"><input type="date" name="pressDate" class="form-control" value="<fmt:formatDate value='${book.pressDate}' pattern='yyyy-MM-dd'/>" "></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">版次：</label>
                        <div class="col-sm-8"><input type="text" name="edition" class="form-control" value="${book.edition}"></div>
                    </div>
                </div>

                <label class="control-label col-sm-2">内容简介：</label>
                <div class="col-sm-4"><textarea class="form-control" name="contentAbstract" >${book.contentAbstract}</textarea></div>
            </div>


            <div class="form-group">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-4">印刷时间：</label>
                        <div class="col-sm-8"><input type="date" name="printDate" class="form-control" value="<fmt:formatDate value='${book.printDate}' pattern='yyyy-MM-dd' />"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">印次：</label>
                        <div class="col-sm-8"><input type="text" name="edition" class="form-control" value="${book.edition}"></div>
                    </div>
                </div>

                <label class="control-label col-sm-2">作者简介：</label>
                <div class="col-sm-4"><textarea class="form-control" name="authorAbstract">${book.authorAbstract}</textarea></div>
            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-4">ISBN：</label>
                        <div class="col-sm-8"><input type="text" name="isbn" class="form-control " value="${book.isbn}"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">开本：</label>
                        <div class="col-sm-8"><input type="text" name="sizes" class="form-control" value="${book.sizes}"></div>
                    </div>
                </div>

                <label class="control-label col-sm-2">基本目录：</label>
                <div class="col-sm-4"><textarea class="form-control" name="director" >${book.director}</textarea></div>
            </div>
            <div class="form-group">

            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-4">纸张：</label>
                        <div class="col-sm-8"><input type="text" name="paper" class="form-control" value="${book.paper}"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4">包装：</label>
                        <div class="col-sm-8"><input type="text" name="pack" class="form-control" value="${book.pack}"></div>
                    </div>
                </div>

                <label class="control-label col-sm-2">媒体评论：</label>
                <div class="col-sm-4"><textarea class="form-control" name="mediaCommentary">${book.mediaCommentary}</textarea></div>
            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <input type="submit" class="btn btn-success pull-right" value="提交" />
                </div>
                <div class="col-sm-6">
                    <input type="button" class="btn btn-warning" value="返回上级" onclick="history.go(-1);" />
                </div>
            </div>


        </form>
    </div>
    <div class="panel-footer"></div>
</div>

<script>
    function upload_review() {
        var img = document.getElementById("uploadimg");
        var input = document.getElementById("file_upload1");
        var tip = document.getElementById("uploadtip");

        var file = input.files.item(0);
        var freader = new FileReader();
        freader.readAsDataURL(file);
        freader.onload = function(e) {
            img.src = e.target.result;
            tip.style.display = "none";
        };
    }
</script>
</body>
</html>
