<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Xsy
  Date: 2020/7/18
  Time: 21:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
</head>
<body>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4>订单管理</h4>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>订单编号</th>
                <th>金额</th>
                <th>订单状态</th>
                <th>收件人</th>
                <th>收货地址</th>
                <th>创建日期</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="o" items="${pageInfo.list}">
                <tr>
                    <td>${o.orderId}</td>
                    <td>${o.total}</td>
                    <td>${o.status}</td>
                    <td>${o.name}</td>
                    <td>${o.city}</td>
                    <td><fmt:formatDate value="${o.createDate}" pattern="yyyy-MM-dd"/> </td>
                    <td>
                        <a href="${pageContext.request.contextPath}/back/order/showItems.do?orderId=${o.orderId}" class="btn btn-info" >订单详细信息</a>
                    </td>
                </tr>
            </c:forEach>


            </tbody>
        </table>
        <ul class="pagination">
            <c:if test="${pageInfo.pageNum == 1}">
                <li class="disabled">
                    <a href="javascript:void(0)" aria-label="Previous" >
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            </c:if>
            <c:if test="${pageInfo.pageNum > 1}">
                <li>
                    <a href="${pageContext.request.contextPath}/back/order/show.do?pageNum=${pageInfo.pageNum-1}&pageSize=${pageInfo.pageSize}" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            </c:if>
            <c:forEach var="i"  items="${requestScope.pageInfo.navigatepageNums}">
                <li ${pageInfo.pageNum == i? "class='active'":""}><a href="${pageContext.request.contextPath}/back/order/show.do?pageNum=${i}&pageSize=${pageInfo.pageSize}">${i}</a></li>
            </c:forEach>
            <!--
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            -->
            <c:if test="${pageInfo.isLastPage}">
                <li class="disabled">
                    <a href="javascript:void(0)" aria-label="Previous" class="disabled">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </c:if>
            <c:if test="${!pageInfo.isLastPage}">
                <li>
                    <a href="${pageContext.request.contextPath}/back/order/show.do?pageNum=${pageInfo.pageNum+1}&pageSize=${pageInfo.pageSize}" aria-label="Previous">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </c:if>
        </ul>
    </div>
    <div class="panel-footer"></div>
</div>
<script>
    console.log(document.body.scrollHeight);
    $(function(){
        $(".collapse").on('shown.bs.collapse',function(){
            console.log("shown");
            console.log(document.body.scrollHeight);
            $(parent.document).find("#content").prop("height",document.body.scrollHeight);
        })

        $(".collapse").on("hidden.bs.collapse",function(){
            console.log("hidden");
            console.log(document.body.scrollHeight);
            $(parent.document).find("#content").prop("height",document.body.scrollHeight);
        })
    })
</script>
</body>
</html>
