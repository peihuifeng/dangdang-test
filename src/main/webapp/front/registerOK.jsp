<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2022/3/10 0010
  Time: 15:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
</head>
<body>
<div class="container" style="margin-top:20px">
    <div class="row">
        <div class="col-md-5">
            <img src="images/DDlogoNEW.gif" alt="" class="pull-right">
        </div>
    </div>

    <hr style="border:solid 2px red">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <ul class="nav nav-pills " style="padding-top:10px">
                <li  style="margin: 10px;background:rgb(246,246,246)"><h5 style="margin:15px;" class="text-muted">1.填写信息</h5></li>
                <li  style="margin: 10px;background:rgb(246,246,246)"><h5 style="margin:15px" class="text-muted">2.验证邮箱</h5></li>
                <li  style="margin:10px;background:rgb(255,102,102)"><h5 style="margin:15px;color:rgb(255,231,250)" >3.注册成功</h5></li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-md-offset-3" >
            <div class="panel panel-default" >
                <div class="panel-heading" style="background: #E5F8E4">
                    <h3 class="panel-title"><span style="font-weight:bolder">注册成功</span></h3>
                </div>
                <div class="panel-body" style="padding:0 10px;background: #E5F8E4">
                    <p style="margin-top:10px"><span>${user.nickname}</span>,欢迎加入当当网</p>
                    <p class="text-muted">请牢记您的登录邮箱地址: <span>${user.email}</span></p>
                    <p class="text-muted"><span>5</span> 秒后自动跳转至当当网首页</p>
                    <p>您现在可以：</p>
                    <ul>
                        <li>进入 <a href="${pageContext.request.contextPath}front/main.jsp">我的当当</a> 查看并管理您的个人信息</li>
                        <li><a href="${pageContext.request.contextPath}front/main.jsp">浏览并选购商品</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
