<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Xsy
  Date: 2020/7/14
  Time: 21:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
</head>
<body>
<div class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a href="http://www.baizhiedu.com" class="navbar-brand">当当网</a>

        </div>
        <span class="navbar-text">欢迎您：<a href="" >xxx</a></span>
        <ul class="nav navbar-nav">
            <li><a href="" class="navbar-link"><span class="text-primary">登录</span></a></li>
            <li class="divider"></li>
            <li><a href="" class="navbar-link"><span class="text-primary">注册</span></a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li >
                <a href="cart/cart.html">
                    <svg height="15"  version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 489 489" style="enable-background:new 0 0 489 489;" xml:space="preserve">
											<g>
                                                <path d="M440.1,422.7l-28-315.3c-0.6-7-6.5-12.3-13.4-12.3h-57.6C340.3,42.5,297.3,0,244.5,0s-95.8,42.5-96.6,95.1H90.3
													c-7,0-12.8,5.3-13.4,12.3l-28,315.3c0,0.4-0.1,0.8-0.1,1.2c0,35.9,32.9,65.1,73.4,65.1h244.6c40.5,0,73.4-29.2,73.4-65.1
													C440.2,423.5,440.2,423.1,440.1,422.7z M244.5,27c37.9,0,68.8,30.4,69.6,68.1H174.9C175.7,57.4,206.6,27,244.5,27z M366.8,462
													H122.2c-25.4,0-46-16.8-46.4-37.5l26.8-302.3h45.2v41c0,7.5,6,13.5,13.5,13.5s13.5-6,13.5-13.5v-41h139.3v41
													c0,7.5,6,13.5,13.5,13.5s13.5-6,13.5-13.5v-41h45.2l26.9,302.3C412.8,445.2,392.1,462,366.8,462z"/>
                                            </g>
										</svg>
                    <span class="text-muted">购物车</span>
                </a>
            </li>
            <li class="divider"></li>
            <li><a href="">我的当当</a></li>
            <li class="divider"></li>
            <li><a href="">帮助</a></li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <img src="images/DDlogoNEW.gif" alt="">
        </div>
        <div class="col-md-6">
            <form action="${pageContext.request.contextPath}/front/searchBook.do">
                <div class="input-group navbar-btn">
                    <input type="text" name="value" placeholder="搜索图书名" id="" class="form-control">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-3">
            <ul class="nav nav-pills">
                <li class="bg-success"><a href=""><span class="text-muted">购物车</span><span class="badge">0</span></a></li>
                <li class="bg-info"><a href=""><span class="text-muted">我的订单</span></a></li>
            </ul>
        </div>
    </div>

    <div class="navbar navbar-default">
        <div class="container-fluid">
            <ol class="breadcrumb navbar-left navbar-btn">
                <span>你当前所在的位置：</span>
                <li><a href="main.html">当当图书</a></li>
                <li >搜索</li>
                <li class="active">${value}</li>
            </ol>

            <ul class="pager navbar-right navbar-btn">
                <li>第 <span>${pageInfo.pageNum}</span> 页</li>
                <c:if test="${pageInfo.pageNum == 1}">
                    <li class="disabled"><a href="javascript:void(0)">上一页</a></li>
                </c:if>
                <c:if test="${pageInfo.pageNum > 1}">
                    <li><a href="${pageContext.request.contextPath}/front/searchBook.do?value=${value}&pageNum=${pageInfo.pageNum-1}&pageSize=${pageInfo.pageSize}">上一页</a></li>
                </c:if>

                <c:if test="${pageInfo.pageNum < pageInfo.pages}">
                    <li><a href="${pageContext.request.contextPath}/front/searchBook.do?value=${value}&pageNum=${pageInfo.pageNum+1}&pageSize=${pageInfo.pageSize}">下一页</a></li>
                </c:if>
                <c:if test="${pageInfo.pageNum > pageInfo.pages}">
                    <li><a href="javascript:void(0)">下一页</a></li>
                </c:if>
                <li>共 <span>${pageInfo.pages}</span> 页</li>
            </ul>

        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <ul class="media-list">
                        <c:forEach var="b" items="${pageInfo.list}">
                            <li class="media">
                                <div class="media-left media-middle">
                                    <a href="${pageContext.request.contextPath}/front/showOneBook.do?id=${b.bookId}"><img class="media-object" src="${pageContext.request.contextPath}/images/book/${b.cover}" alt="" style="width:150px"></a>
                                </div>
                                <div class="media-body">
                                    <a href="${pageContext.request.contextPath}/front/showOneBook.do?id=${b.bookId}"><h5 class="text-primary">${b.bookName}</h5></a>
                                    <hr>
                                    <p class="small text-muted">作者：<span>${b.author}</span></p>
                                    <p class="small text-muted">出版社：<span>${b.press}</span>
                                        出版时间：<span><fmt:formatDate value="${b.pressDate}" pattern="yyyy-MM-dd"/> </span></p>
                                    <p>${b.editorRecommend}</p>
                                    <p><del class="">￥${b.price}</del> <strong style="color:red">￥${b.dprice}</strong> 节省：<span class="text-success">￥${b.price - b.dprice}</span>
                                        <a href="${pageContext.request.contextPath}/front/cart/addCart.do?id=${b.bookId}" class="btn btn-danger">加入购物车</a>
                                    </p>
                                </div>
                            </li>
                            <hr>
                        </c:forEach>

                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>
