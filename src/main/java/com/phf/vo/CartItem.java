package com.phf.vo;

import com.phf.entity.Book;
import lombok.Data;

import java.io.Serializable;
@Data
public class CartItem implements Serializable {
    private Book book;
    private Integer count;

    public CartItem() {
    }

}
