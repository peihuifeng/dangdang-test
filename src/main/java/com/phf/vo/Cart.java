package com.phf.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
public class Cart implements Serializable {
    private Map<Integer, CartItem> items = new HashMap<>();

    public int getSize(){
        return items.size();
    }
    public double getTotalPrice(){
        return items.values().stream().mapToDouble(item->item.getBook().getDprice()*item.getCount()).sum();
    }
    public double getSavePrice(){
        return items.values().stream().mapToDouble(item->(item.getBook().getPrice()-item.getBook().getDprice()) * item.getCount()).sum();
    }
}
