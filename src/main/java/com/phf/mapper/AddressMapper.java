package com.phf.mapper;

import com.phf.entity.Address;

import java.util.List;

public interface AddressMapper {
    public void insertAddress(Address Address);
    public void deleteAddress(Integer id);
    public void updateAddress(Address Address);
    public Address selectAddress(Integer id);
    public List<Address> selectAddresses(Integer userId);
}
