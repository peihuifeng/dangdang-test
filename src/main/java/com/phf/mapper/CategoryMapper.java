package com.phf.mapper;

import com.phf.entity.Category;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CategoryMapper {
    public List<Category> selectCategoriesWithParent(@Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);
    void insertCategory(Category category);
    List<Category> selectCategoriesByLevels(Integer levels);
    public void deleteCategory(Integer id);
    public int countRowsByParentId(Integer parentId);
    public List<Category> selectCategoriesWithChildren();
}
