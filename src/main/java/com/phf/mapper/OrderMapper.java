package com.phf.mapper;

import com.phf.entity.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderMapper {
    public void insertOrder(Order order);
    public Order selectOrder(Integer id);
    public void updateOrder(Order order);
    public List<Order> showOrders(@Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize);
}
