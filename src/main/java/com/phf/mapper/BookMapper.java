package com.phf.mapper;

import com.phf.entity.Book;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookMapper {
    public int countRowsByCategoryId(Integer categoryId);

    public void insertBook(Book b);
    public void deleteBook(Integer id);

    public void updateBook(Book b);
    public List<Book> selectSecondBooks(@Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize,@Param("categoryId")Integer categoryId);

    public List<Book> selectBooks(@Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize, @Param("query") String query, @Param("value") String value);

    public Book selectBook(Integer id);

    public List<Book> selectRecommend();
    public List<Book> selectHots();
    public List<Book> selectNews();
    public List<Book> selectNewHots();
}
