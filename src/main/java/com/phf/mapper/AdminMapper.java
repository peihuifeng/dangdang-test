package com.phf.mapper;

import com.phf.entity.Admin;

import java.util.List;

public interface AdminMapper {
    public void  insertAdmin(Admin admin);
    public  void deleteAdmin(Integer id);
    public void updateAdmin(Admin admin);
    public Admin selectAdmin(Integer id);
    public Admin selectAdminByUsername(String username);
    public List<Admin> selectAdmins();
}
