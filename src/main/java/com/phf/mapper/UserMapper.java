package com.phf.mapper;

import com.phf.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    public void  insertUser(User user);
    public void  deleteUser(Integer id);
    public void updateUser(User user);
    public User selectUserByEmail(String email);
    public List<User> selectUsers(@Param("pageNum") Integer pageNum,@Param("pageSize") Integer pageSize);

}
