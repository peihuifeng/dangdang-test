package com.phf.mapper;

import com.phf.entity.Item;
import com.phf.entity.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ItemMapper {
    public void insertItem(Item item);
    public List<Item> showItems(@Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize,@Param("orderId")Integer orderId);
}
