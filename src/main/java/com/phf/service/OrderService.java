package com.phf.service;

import com.github.pagehelper.PageInfo;
import com.phf.entity.Order;
import com.phf.entity.User;
import com.phf.vo.CartItem;

import java.util.Collection;

public interface OrderService {
    public void addOrder(Order order, Collection<CartItem> items);

    public Order showOneOrder(Integer id);
    public  void  updateOrder(Order order);
    public PageInfo<Order> showOrders(int pageNum, int pageSize);
}
