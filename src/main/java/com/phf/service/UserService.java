package com.phf.service;

import com.github.pagehelper.PageInfo;
import com.phf.entity.User;

public interface UserService {
    public User login(String email,String password);
    public PageInfo<User> showUsers(int pageNum,int pageSize);
    public void updateUser(User user);
    public void addUser(User user);
    public void removeUser(Integer id);
}
