package com.phf.service;

import com.phf.entity.Category;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface CategoryService {
    PageInfo<Category> showCategoriesWithParent(int pageNum, int pageSize);
    void addCategory(Category c);
    List<Category> showCategoriesByLevels(Integer levels);
    void removeCategory(Integer id);
    List<Category> showCategoriesWithChildren();
}
