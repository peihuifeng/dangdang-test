package com.phf.service;

import com.github.pagehelper.PageInfo;
import com.phf.entity.Item;
import com.phf.entity.Order;

public interface ItemService {
    public void addItem(Item item);
    public PageInfo<Item> showItems(Integer pageNum, Integer pageSize,Integer orderId);
}
