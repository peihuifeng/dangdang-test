package com.phf.service;

import com.phf.entity.Address;

import java.util.List;

public interface AddressService {
    public List<Address> showAddresses(Integer userId);
    public void addAddress(Address address);
    public void removeAddress(Integer id);
    public void updateAddress(Address address);
    public Address showOneAddress(Integer id);
}
