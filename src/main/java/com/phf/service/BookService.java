package com.phf.service;

import com.phf.entity.Book;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface BookService {
    public PageInfo<Book> showBooks(int pageNum, int pageSize, String query, String value);

    public void addBook(Book b);
    public  void deleteBook(Integer id);
    public void updateBook(Book b);

    public Book showOneBook(Integer id);

    public List<Book> showRecommend();
    public List<Book> showHots();
    public List<Book> showNews();
    public List<Book> showNewHots();
    public PageInfo<Book> searchBooks(Integer pageNum, Integer pageSize, String value);
    public PageInfo<Book> selectSecondBooks(Integer pageNum, Integer pageSize, Integer categoryId);
}
