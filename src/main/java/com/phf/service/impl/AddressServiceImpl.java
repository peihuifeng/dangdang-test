package com.phf.service.impl;

import com.phf.entity.Address;
import com.phf.mapper.AddressMapper;
import com.phf.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AddressServiceImpl implements AddressService {
    @Autowired
    private AddressMapper addressMapper;
    @Override
    public List<Address> showAddresses(Integer userId) {
        return addressMapper.selectAddresses(userId);
    }

    @Override
    public void addAddress(Address address) {
        addressMapper.insertAddress(address);
    }

    @Override
    public void removeAddress(Integer id) {
        addressMapper.deleteAddress(id);
    }

    @Override
    public void updateAddress(Address address) {
        addressMapper.updateAddress(address);
    }

    @Override
    public Address showOneAddress(Integer id) {
        return addressMapper.selectAddress(id);
    }
}
