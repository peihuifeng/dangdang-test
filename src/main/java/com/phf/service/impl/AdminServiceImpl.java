package com.phf.service.impl;

import com.phf.entity.Admin;
import com.phf.mapper.AdminMapper;
import com.phf.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminMapper adminMapper;
    @Override
    public boolean login(String username, String password) {
        Admin admin = adminMapper.selectAdminByUsername(username);
        if(admin ==null){
            return false;
        }
        if (!admin.getPassword().equals(password)) {
            return false;
        }
        return true;
    }
}
