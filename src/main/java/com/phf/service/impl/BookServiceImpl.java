package com.phf.service.impl;

import com.phf.entity.Book;
import com.phf.mapper.BookMapper;
import com.phf.service.BookService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    private BookMapper bookMapper;

    public PageInfo<Book> showBooks(int pageNum, int pageSize, String query, String value) {
        return new PageInfo<Book>(bookMapper.selectBooks(pageNum,pageSize,query,value),10);
    }


    public void addBook(Book b) {
        bookMapper.insertBook(b);
    }


    public void deleteBook(Integer id) {bookMapper.deleteBook(id); }


    public void updateBook(Book b) {
        bookMapper.updateBook(b);
    }


    public Book showOneBook(Integer id) {
        return bookMapper.selectBook(id);
    }


    public List<Book> showRecommend() {
        return bookMapper.selectRecommend();
    }


    public List<Book> showHots() {
        return bookMapper.selectHots();
    }


    public List<Book> showNews() {
        return bookMapper.selectNews();
    }


    public List<Book> showNewHots() {
        return bookMapper.selectNewHots();
    }


    public PageInfo<Book> searchBooks(Integer pageNum,Integer pageSize,String value) {
        return new PageInfo<Book>(bookMapper.selectBooks(pageNum,pageSize,null,value),10);
    }

    @Override
    public PageInfo<Book> selectSecondBooks(Integer pageNum, Integer pageSize, Integer categoryId) {
        return new PageInfo<Book>(bookMapper.selectSecondBooks(pageNum,pageSize,categoryId),10);
    }


}

