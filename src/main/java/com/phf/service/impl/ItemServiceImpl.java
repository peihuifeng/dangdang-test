package com.phf.service.impl;

import com.github.pagehelper.PageInfo;
import com.phf.entity.Item;
import com.phf.entity.Order;
import com.phf.mapper.ItemMapper;
import com.phf.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemServiceImpl implements ItemService {
    @Autowired
    private ItemMapper itemMapper;
    @Override
    public void addItem(Item item) {
        itemMapper.insertItem(item);
    }

    @Override
    public PageInfo<Item> showItems(Integer pageNum, Integer pageSize,Integer orderId) {
        return new PageInfo<Item>(itemMapper.showItems(pageNum,pageSize,orderId),6);
    }
}
