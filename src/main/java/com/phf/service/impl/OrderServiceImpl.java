package com.phf.service.impl;

import com.github.pagehelper.PageInfo;
import com.phf.entity.Item;
import com.phf.entity.Order;
import com.phf.entity.User;
import com.phf.mapper.ItemMapper;
import com.phf.mapper.OrderMapper;
import com.phf.service.OrderService;
import com.phf.vo.CartItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;

@Service

public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private ItemMapper itemMapper;
    @Override
    public void addOrder(Order order, Collection<CartItem> items) {
        orderMapper.insertOrder(order);
        for(CartItem cartItem:items){
            Item item = new Item();
            item.setBookId(cartItem.getBook().getBookId());
            item.setCount(cartItem.getCount());
            item.setCreateDate(new Date());
            item.setOrderId(order.getOrderId());
            itemMapper.insertItem(item);
        }
    }

    @Override
    public Order showOneOrder(Integer id) {
        return orderMapper.selectOrder(id);
    }

    @Override
    public void updateOrder(Order order) {
        orderMapper.updateOrder(order);
    }

    @Override
    public PageInfo<Order> showOrders(int pageNum, int pageSize) {
        return new PageInfo<Order>(orderMapper.showOrders(pageNum,pageSize),10);
    }
}
