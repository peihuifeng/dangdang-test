package com.phf.service.impl;

import com.phf.entity.Category;
import com.phf.mapper.BookMapper;
import com.phf.mapper.CategoryMapper;
import com.phf.service.CategoryService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    public PageInfo<Category> showCategoriesWithParent(int pageNum,int pageSize) {
        List<Category> categories = categoryMapper.selectCategoriesWithParent(pageNum, pageSize);
        return new PageInfo<Category>(categories,10);
    }


    public void addCategory(Category c) {
        categoryMapper.insertCategory(c);
    }


    public List<Category> showCategoriesByLevels(Integer levels) {
        return categoryMapper.selectCategoriesByLevels(levels);
    }

    @Autowired
    private BookMapper bookMapper;

    public void removeCategory(Integer id) {
        int i = categoryMapper.countRowsByParentId(id);
        if(i > 0){
            throw new RuntimeException("该类别下已有子类别,不能删除");
        }
        if(bookMapper.countRowsByCategoryId(id) > 0){
            throw new RuntimeException("该类别下已有图书,不能删除");
        }
        categoryMapper.deleteCategory(id);
    }


    public List<Category> showCategoriesWithChildren() {
        return categoryMapper.selectCategoriesWithChildren();
    }
}
