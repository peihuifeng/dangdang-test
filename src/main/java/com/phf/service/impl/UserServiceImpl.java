package com.phf.service.impl;

import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.crypto.digest.HMac;
import cn.hutool.crypto.digest.HmacAlgorithm;
import com.github.pagehelper.PageInfo;
import com.phf.entity.User;
import com.phf.mapper.UserMapper;
import com.phf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public User login(String email, String password) {
       User user =userMapper.selectUserByEmail(email);
       if(user==null){
           throw new RuntimeException("用户未注册");

       }
       if(user.getStatus()!=1){
           throw  new RuntimeException("账户未激活，请自行激活或联系系统管理员");

       }
//       HMac hmac = DigestUtil.hmac(HmacAlgorithm.HmacMD5,user.getSalt().getBytes());
//       String digestPwd=hmac.digestHex(password);
       if(!password.equals(user.getPassword())){
           throw new RuntimeException("用户名或密码错误");
       }
        return user;
    }

    @Override
    public PageInfo<User> showUsers(int pageNum, int pageSize) {
        return new PageInfo<User>(userMapper.selectUsers(pageNum,pageSize),10);
    }

    @Override
    public void updateUser(User user) {
        userMapper.updateUser(user);
    }

    @Override
    public void addUser(User user) {
        userMapper.insertUser(user);
    }

    @Override
    public void removeUser(Integer id) {
        userMapper.deleteUser(id);
    }
}
