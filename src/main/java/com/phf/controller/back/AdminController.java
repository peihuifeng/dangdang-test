package com.phf.controller.back;

import com.phf.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("back")
public class AdminController {
    @Autowired
    private AdminService adminService;

    @RequestMapping("login")
    public String login(String username, String password, String validateCode, HttpSession httpSession){
        System.out.println("username = " + username + ", password = " + password + ", validateCode = " + validateCode + ", httpSession = " + httpSession);
        //验证码判断
        if (!validateCode.equalsIgnoreCase((String) httpSession.getAttribute("realCode"))) {
            httpSession.setAttribute("msg","验证码错误");
            return "redirect:/back/login.jsp";
        }
        boolean login = adminService.login(username, password);
        if (login) {
            return "redirect:/back/manager.jsp";
        }else{
            httpSession.setAttribute("msg","用户名或密码错误");
            return "redirect:/back/login.jsp";
        }
    }
}
