package com.phf.controller.back;

import com.phf.entity.Book;
import com.phf.entity.Category;
import com.phf.service.BookService;
import com.phf.service.CategoryService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("back/book")
public class BookController {
    @Autowired
    private BookService bookService;

    @RequestMapping("show")
    public String show(@RequestParam(defaultValue = "1")Integer pageNum,
                       @RequestParam(defaultValue = "5")Integer pageSize,
                       @RequestParam(defaultValue = "")String query,
                       @RequestParam(defaultValue = "")String value,
                       Model model){
        PageInfo<Book> pageInfo = bookService.showBooks(pageNum, pageSize, query, value);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("query",query);
        model.addAttribute("value",value);


        return "forward:/back/book/show.jsp";
    }

    @RequestMapping("add")
    public String addBook(Book b, MultipartFile pic,HttpServletRequest req) throws IOException {
        String realPath = req.getServletContext().getRealPath("/images/book/");
        FileCopyUtils.copy(pic.getInputStream(), new FileOutputStream(realPath + pic.getOriginalFilename()));

        System.out.println("b = " + b + ", pic = " + pic + ", req = " + req);
        b.setCover(pic.getOriginalFilename());
        b.setCreateDate(new Date());
        b.setSale(0);

        System.out.println("b = " + b);
        bookService.addBook(b);
        return "redirect:/back/book/show.do";

    }

    @Autowired
    private CategoryService categoryService;
    @RequestMapping("showOne")
    public String showBook(Integer id,Model model){
        Book book = bookService.showOneBook(id);
        model.addAttribute("book",book);
        List<Category> categories = categoryService.showCategoriesByLevels(2);
        model.addAttribute("categories",categories);
        return "forward:/back/book/update.jsp";
    }

    @RequestMapping("update")
    public String updateBook(Book b,MultipartFile pic,HttpServletRequest req) throws IOException {
        //如果用户更新了图片,上传图片
        if (pic != null && pic.getOriginalFilename() != null && !pic.getOriginalFilename().isEmpty()) {
            String realPath = req.getServletContext().getRealPath("/images/book/");
            FileCopyUtils.copy(pic.getInputStream(), new FileOutputStream(realPath + pic.getOriginalFilename()));
            b.setCover(pic.getOriginalFilename());
        }

        bookService.updateBook(b);

        return "redirect:/back/book/show.do";

    }
        @RequestMapping("remove")
        public String removeBook(Integer id){
        bookService.deleteBook(id);
        return "redirect:/back/book/show.do";
    }
}

