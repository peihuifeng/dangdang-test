package com.phf.controller.back;

import com.phf.entity.User;
import com.phf.service.UserService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("back/user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("show")
    public String show(@RequestParam(defaultValue = "1") Integer pageNum,
                       @RequestParam(defaultValue = "5") Integer pageSize,
                       Model model){
        PageInfo<User> pageInfo = userService.showUsers(pageNum,pageSize);
        model.addAttribute("pageInfo",pageInfo);
        return "forward:/back/user/show.jsp";
    }

    @RequestMapping("updateStatus")
    public String updateStatus(User user){
        userService.updateUser(user);
        return "redirect:/back/user/show.do";
    }
}
