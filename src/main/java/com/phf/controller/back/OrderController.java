package com.phf.controller.back;

import com.github.pagehelper.PageInfo;
import com.phf.entity.Book;
import com.phf.entity.Item;
import com.phf.entity.Order;
import com.phf.entity.User;
import com.phf.service.ItemService;
import com.phf.service.OrderService;
import com.phf.vo.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Date;

@Controller("backOrderController")
@RequestMapping("back/order")
public class OrderController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private ItemService itemService;
    @RequestMapping("show")
    public String show(@RequestParam(defaultValue = "1") Integer pageNum,
                       @RequestParam(defaultValue = "5") Integer pageSize,
                       Model model){
        PageInfo<Order> pageInfo = orderService.showOrders(pageNum,pageSize);
        model.addAttribute("pageInfo",pageInfo);
        return "forward:/back/order/show.jsp";
    }
    @RequestMapping("showItems")
    public String showItems(@RequestParam(defaultValue = "1") Integer pageNum,
                       @RequestParam(defaultValue = "5") Integer pageSize,
                        Integer orderId,Model model){
        System.out.println("---------------------------"+orderId);
        PageInfo<Item> pageInfo = itemService.showItems(pageNum,pageSize,orderId);
        System.out.println(pageInfo);
        model.addAttribute("pageInfo",pageInfo);


        System.out.println("____________-_____");
        return "forward:/back/order/detail.jsp";
    }





}