package com.phf.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/validateCode")
public class ValidateCodeServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LineCaptcha captcha= CaptchaUtil.createLineCaptcha(200,100);
        HttpSession session=req.getSession();
        session.setAttribute("realCode",captcha.getCode());
        ServletOutputStream out=resp.getOutputStream();
        captcha.write(out);
        System.out.println(captcha.getCode());
    }
}
