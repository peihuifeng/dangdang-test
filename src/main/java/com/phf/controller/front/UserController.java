package com.phf.controller.front;

import com.phf.entity.User;
import com.phf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import sun.misc.Request;

import javax.mail.Session;
import javax.servlet.http.HttpSession;
import java.util.Date;

@Controller("frontUserController")
@RequestMapping("front")
public class UserController {
    @Autowired
    private UserService userService;
    @RequestMapping("login")
    public String login(String email, String password, HttpSession session){
        try {
            User user = userService.login(email, password);
            session.setAttribute("user",user);
            return "redirect:/front/main.do";
        }catch(Exception e){
            e.printStackTrace();
            session.setAttribute("msg",e.getMessage());
            return "redirect:/front/login.jsp";
        }

    }
    @RequestMapping("register")
    public String register(User user,String validateCode, HttpSession httpSession) {
        user.setCreateDate(new Date());
        System.out.println("user= " + user + ", validateCode = " + validateCode + ", httpSession = " + httpSession);
        if (!validateCode.equalsIgnoreCase((String) httpSession.getAttribute("realCode"))) {
            httpSession.setAttribute("msg", "验证码错误");
            return "redirect:/front/register.jsp";
        } else {
            userService.addUser(user);
            httpSession.setAttribute("user", user);
            return "redirect:/front/registerOK.jsp";

        }
    }
}



