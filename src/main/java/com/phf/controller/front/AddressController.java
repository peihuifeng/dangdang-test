package com.phf.controller.front;

import com.phf.entity.Address;
import com.phf.entity.User;
import com.phf.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.SessionScope;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("front/address")
public class AddressController {
    @Autowired
    private AddressService addressService;

    @RequestMapping("showAddress")
    public String showAddress(Integer userId, Model model){
        List<Address> addresses = addressService.showAddresses(userId);
        model.addAttribute("address",addresses);
        return "forward:/front/order/place-order.jsp";
    }

    @RequestMapping("addAddress")
    public String addAddress(Address address, HttpSession session){
        System.out.println("address = " + address + ", session = " + session);
        User user = (User)session.getAttribute("user");
        address.setUserId(user.getUserId());
        addressService.addAddress(address);

        return "redirect:/front/address/showAddress.do?userId="+user.getUserId();
    }
    @RequestMapping("updateAddress")
    public  String updateAddress(Address address,HttpSession session){
        System.out.println("address = " + address + ", session = " + session);
        User user = (User) session.getAttribute("user");
        address.setUserId(user.getUserId());
        addressService.updateAddress(address);
        return "redirect:/front/address/showAddress.do?userId="+user.getUserId();
    }
    @RequestMapping("removeAddress")
    public String  deleteAddress(Integer id,HttpSession session){
        System.out.println("____________________");
        System.out.println("____________________");
        System.out.println("____________________");
        System.out.println("id = " + id + ", session = " + session);
        User user = (User) session.getAttribute("user");
        addressService.removeAddress(id);
        return "redirect:/front/address/showAddress.do?userId="+user.getUserId();
    }


}
