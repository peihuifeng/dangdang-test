package com.phf.controller.front;


import com.github.pagehelper.PageInfo;
import com.phf.entity.Book;
import com.phf.entity.Category;
import com.phf.service.BookService;
import com.phf.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@RequestMapping("front")
@Controller
public class PageController {
@Autowired
    private CategoryService categoryService;
@Autowired
    private BookService bookService;
    @RequestMapping("main")
    public String main(Model model){
        List<Category> categories = categoryService.showCategoriesWithChildren();
        model.addAttribute("categories", categories);

        List<Book> recommendBooks=bookService.showRecommend();
        List<Book> hotSaleBooks=bookService.showHots();
        List<Book> newBooks=bookService.showNewHots();
        List<Book> newHotSaleBooks = bookService.showNewHots();

        model.addAttribute("recommendBooks", recommendBooks);
        model.addAttribute("hotSaleBooks", hotSaleBooks);
        model.addAttribute("newBooks", newBooks);
        model.addAttribute("newHotSaleBooks", newHotSaleBooks);

        return "forward:/front/main.jsp";
    }
@RequestMapping("searchBook")
    public String searchBook(@RequestParam(defaultValue = "1") Integer pageNum,
                             @RequestParam(defaultValue = "5") Integer pageSize,
                             String value,Model model){
    PageInfo<Book> pageInfo=bookService.searchBooks(pageNum,pageSize,value);
    model.addAttribute("pageInfo",pageInfo);
    model.addAttribute("value",value);
    return "forward:/front/search-book.jsp";

    }
    @RequestMapping("showOneBook")
    public String showOneBook(Integer id,Model model){
        Book book= bookService.showOneBook(id);
        model.addAttribute("book",book);
        return"forward:/front/book-detail.jsp";

    }
    @RequestMapping("secondPage")
    public String showSecondPage(@RequestParam(defaultValue = "1") Integer pageNum,
                                 @RequestParam(defaultValue = "5") Integer pageSize,
                                 Integer categoryId,Model model){
        System.out.println("------------------"+categoryId);
        System.out.println("------------------"+categoryId);
        System.out.println("------------------"+categoryId);
        System.out.println("------------------"+categoryId);
        model.addAttribute("categoryId",categoryId);
        PageInfo<Book> pageInfo=bookService.selectSecondBooks(pageNum,pageSize,categoryId);

        model.addAttribute("pageInfo",pageInfo);

        return "forward:/front/search-book.jsp";
    }
}
