package com.phf.controller.front;

import com.phf.entity.Address;
import com.phf.entity.Order;
import com.phf.entity.User;
import com.phf.service.ItemService;
import com.phf.service.OrderService;
import com.phf.vo.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.Date;

@Controller
@RequestMapping("front/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @RequestMapping("addOrder")
    public String addOrder(Order order, HttpSession session) {
        System.out.println("order = " + order);
        User user = (User) session.getAttribute("user");
        order.setUserId(user.getUserId());
        order.setStatus("未支付");
        order.setCreateDate(new Date());

        Cart cart = (Cart) session.getAttribute("cart");
        orderService.addOrder(order, cart.getItems().values());

        cart.getItems().clear();

        return "redirect:/front/order/showOneOrder.do?id=" + order.getOrderId();
    }

    @RequestMapping("showOneOrder")
    public String showOneOrder(Integer id, Model model) {

        Order order = orderService.showOneOrder(id);
        model.addAttribute("order", order);
        return "forward:/front/pay/pay_order.jsp";
    }

    //    @RequestMapping("updateStatus")
//    public String updateStatus(User user){
//        userService.updateUser(user);
//        return "redirect:/back/user/show.do";
//    }
    @RequestMapping("updateOrder")
    public String updateOrder(Order order) {
//        order.setOrderId(order.getOrderId());
//        order.setTotal(order.getTotal());
             order.setStatus("支付成功");
//        order.setName(order.getName());
//        order.setMobile(order.getMobile());
//        order.setProvince(order.getProvince());
//        order.setCity(order.getCity());
//        order.setDistrict(order.getDistrict());
//        order.setTown(order.getTown());
//        order.setAddress(order.getAddress());
//        order.setCreateDate(order.getCreateDate());
//        order.setUserId(order.getUserId());

        orderService.updateOrder(order);
        return"redirect:/front/main.do";
    }
}