package com.phf.alipay.config;

import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {

//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2021000119631169";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCPzylhDtsfA1E9Jll0ZV4/AyUhRZko0hvnbX0syhWl4T9eCWg6ziKYd/n8LVj1I3LhEk0hl14ynu6jkV+xiRUR0buRgFdNqrAefNTY9X8qsw9F30OVUsp8Zn02IOfYliqJjSDTZ20KcHcG/KVFuRjebjZLw9p3deBlVfBa8KQDIAtjLfmqD/HQuFVrWJUiNJhIMlt2calASPFKnlgYO9HBF+vEMe9eFENoH9E0x9NhZi4u7sQ4zZEyxHuAFNsSk+C9MrL15axlc+0g8lq9xPEn4nhKCwBLA+7+AWs9SGC4NM0ZYpDKwabPRDdejGdkgrSyssGOiotcZ748w/OhJaUZAgMBAAECggEAE1arXNbvlnsOuKdojiN3ige1PYUrdHbUs8lgdN50DMt+hvnC322K72/q2QOMG+L5dtTMNIEzdKE0vCA3extRDwViMM3Ob65NYrwsr0RMJoTPv/inEPSCMDpkChxgsRp4e+jTijo1SpsXFoRxATBD7N1rRcL/pOLgGyB0YvZLR/QZSFRHLppQgZU23pEeT1n0xvgqKb10DUEmWwsGF+tofiyP6h4hjMUb/lSJxjSFPYE9L8g4U/l8fuM2bnHLZYYIop8xEgpzEL28DP5IrbX318hA+87EbxQ/F2Z+m7tJxDLtBsx7GPztOOYWxQslgBgLcbMPJNNNxJjXpJwqXgj50QKBgQDwXsJ4BtiWtAg5me15LcAVlRFsppCJUiCFXBuLm8QYqZRLjlQ4sbgc+BWTKO+IlZ6yaDzHWf9NPL8938l9Yh/7rNPJ956ha/eZOd3vT4cuQ4xvRJjY3RgTQnhtBXjwlXFJoBTheGTwozCnde+S1umaSvYllDyDYLpjlfJaUsngtQKBgQCZKQh+9/a//61WAe3MujTGh2nAmXRgBMfKTZLyDAIf7xPnlzRflxtSZ09nEcqKHrKFEiZwIIv221V2R5qzXyKG152xGSG9/eghgyUaTLrpvIIqe5di9AQjM22hK7NGq49HWjyFPyG1glcFbtiJg+FUc1dhrojdfSBZIMSOlOCFVQKBgFiKv13RP8U7qe/16IkDtkb88z8vl/xNeVdGl4r0pOZkyRCP7Cw6ItTg+amMobeRTbTwOrtARo/f+Z5EKgd6tsdRYW57PT6+86TIsRzvEZ+UGE4AWcFAqBaoHI4kPul4UxPI2Qg4/g26Pzjgc5CGg+RnCobLNQM8BUybtntZFa4lAoGAFh9BJOW39D9/z95zdJCossw4UCNaFcxa9zOBOAjX/bjQjTDf1bbDsYFOjLqbIJwSePoIJ0XsMXm4vF7XT3U8TXGzSzMwFnsoHj8NCuFuVfhubKhxU0+fkV4KRa9EM77v8QRtnWli+w6adyRtjl9IGqSWSHunNSavl3AOM/H5BAkCgYA1Ias4yWPUwBU66wLOaPjh2AfOH0gS/s2sfWnmjb9juoOIwbIjm0uohpEne2GcdFSmhXX0zpR1ukh24zPINXR1W5QoK09NDZUQLp/aXQGgw0GdhZ2GdrS1CNXz2KOY8ycaWXaHhRGsMAXlKJvZfab5pXIxertvq61Rf1zcI3Hvig==";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjfkf/EA1nHT261UFt7hMAMpKvcoRCCfkPWQ7OKBV8erdGFOBpflaEEwP5qxv8M39y+KHLRYlMWCkJe0kqOVnLKA7qwJCWIWYy/dmSdENXBzAh6kdTyfgrXXXaPxuvALhVBCmhivwcVyzkB35d1KhkZE11qMcayqkqoPfVPqYuum84zLMI+xFBteWMkb4wU8TcmANFfre/jnu9xKVS+QcBarwjsQ2dN5H3Q38PLVpYedguR3QoF0QuK+mboWOEom4AK3HO1Fm2Irq2/ZsSCAdP6aDCL/7pBCmTP8uHN/2WTENyXtaw1OE0Ggn77E7DGv44F2Arp2ufNyzirpCsvgsJwIDAQAB";
    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=124这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://localhost:8080/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://localhost:8080/dangdang/front/pay/return_url.jsp";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关          沙箱环境下需要替换为：                "https://openapi.alipaydev.com/gateway.do"
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static void main(String[] args) {
        System.out.println(UUID.randomUUID().toString());
    }
}


