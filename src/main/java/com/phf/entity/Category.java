package com.phf.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category implements Serializable {

    private Integer categoryId;
    private String name;
    private Integer levels;
    private Integer parentId;
    private Category parent;
    private List<Category> children;

}
