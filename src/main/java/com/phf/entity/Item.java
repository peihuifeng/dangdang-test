package com.phf.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class Item implements Serializable {
    private Integer itemId;
    private Integer bookId;
    private Integer count;
    private Integer orderId;
    private Date createDate;
    private Book book;

}
