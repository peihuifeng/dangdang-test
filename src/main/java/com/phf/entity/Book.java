package com.phf.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
public class Book implements Serializable {
    private Integer bookId;
    private String bookName;
    private String author;
    private String cover;
    private String press;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date pressDate;
    private String edition;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date printDate;
    private String impression;
    private String isbn;
    private Integer wordNum;
    private Integer pageNum;
    private String sizes;
    private String paper;
    private String pack;
    private Double price;
    private Double dprice;
    private Date createDate;
    private String editorRecommend;
    private String contentAbstract;
    private String authorAbstract;
    private String director;
    private String mediaCommentary;
    private Integer sale;
    private Integer stock;
    private Integer categoryId;


}
