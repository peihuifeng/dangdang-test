package com.phf.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Order implements Serializable {
    private Integer orderId;
    private Double total;
    private String status;
    private String name;
    private String mobile;
    private String province;
    private String city;
    private String district;
    private String town;
    private String address;
    private Integer userId;
    private Date createDate;
    private Item item;


}
