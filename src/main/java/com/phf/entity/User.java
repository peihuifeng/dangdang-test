package com.phf.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
public class User implements Serializable {
    public  Integer userId;
    private String email;
    private String password;
//    private String salt;
    private String nickname;
    private Integer status;
//    private String code;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date createDate;
}
