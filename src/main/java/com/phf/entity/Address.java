package com.phf.entity;

import lombok.Data;

import java.io.Serializable;
@Data
public class Address implements Serializable {

        private Integer addressId;
        private String name;
        private String mobile;
        private String province;
        private String city;
        private String district;
        private String town;
        private String address;
        private Integer userId;

}
