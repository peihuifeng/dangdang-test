package com.phf.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Admin  implements Serializable {

    private Integer adminId;
    private  String username;
    private String password;
}
